#!/bin/bash
export IOT_PATH=$(readlink -f $(dirname ${BASH_SOURCE[0]}))

#setup NCS (Nordic Connect SDK)
export NCS_VERSION=${1:-v2.2.0}

export NCS_PATH=/opt/nordic/ncs
if [ "$(uname)" == "Linux" ]; then export NCS_PATH=$HOME/ncs; fi

export NCS_RELEASE=${NCS_PATH}/toolchains/${NCS_VERSION}
export ZEPHYR_SDK_INSTALL_DIR=${NCS_RELEASE}/opt/zephyr-sdk
export CMAKE_PROJECT_PATH=${CMAKE_PROJECT_PATH}:${ZEPHYR_SDK_INSTALL_DIR}
export ZEPHYR_BASE=${NCS_PATH}/${NCS_VERSION}/zephyr

export PATH=${NCS_RELEASE}/opt/bin:$PATH
#export PATH=${NCS_RELEASE}/usr/local/bin:$PATH
export PATH=${NCS_RELEASE}/usr/bin:$PATH
export PATH=${NCS_RELEASE}/bin:$PATH
if [ "$(uname)" == "Linux" ]; then
	export LD_LIBRARY_PATH=${NCS_RELEASE}/usr/local/lib/:$LD_LIBRARY_PATH
fi
export BOARD=nrf9160dk_nrf9160

echo "---------"
echo "Setting up NCS release: ${NCS_VERSION}"
echo "ZEPHYR_BASE=${ZEPHYR_BASE}"
echo "ZEPHYR_SDK_INSTALL_DIR=${ZEPHYR_SDK_INSTALL_DIR}"
echo "cmake -D BOARD=<board> <source>"
echo "Available boards:"
echo " - nrf9160dk_nrf9160"
echo " - thingy91_nrf9160"
echo "---------"
