Install Nordic SDK
==================

https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrf/getting_started.html##

1. Download nRF Connect for desktop

https://nsscprodmedia.blob.core.windows.net/prod/software-and-other-downloads/desktop-software/nrf-connect-for-desktop/3-12-1/nrfconnect-3.12.1.dmg

2. Open the dmg image that you just dowloaded and move the application to the applications folder

3. Open the "nRF Connect for Desktop" application

4. Install the following: Toochain Manager, Programmer, and LTE Link Monitor 

5. Click open "Toolchain manager" 

6. From within "Toolchain manager" click on install nRF Connect SDK v2.2.0 

5. Install comamnd line tools
https://nsscprodmedia.blob.core.windows.net/prod/software-and-other-downloads/desktop-software/nrf-command-line-tools/sw/versions-10-x-x/10-18-1/nrf-command-line-tools-10.18.1-darwin.dmg

Compilation instructions
========================

mkdir build
cd build
cmake .. 
make 
make flash

